from jata import *
from typing import List, Dict


class Address(Jata):
    door_num: int
    street: str
    city: str
    country: str


class Nyc(Address):
    city = "New York City"
    country = "United States of America"


class Fav(Jata):
    color: str
    qty: int


class Contact(Jata):
    name: str
    age: int
    # `from` is a python keyword, so cannot be an identifier
    # pykw_ is stripped when converting to json
    pykw_from: Address
    favfood: Dict[str, Fav]


class Team(Jata):
    name: str
    members: List[Contact] = MutableDefault(lambda: [])


#   Cannot have member functions. This will crash. Use free functions instead
#   def get_name(self):
#       return self.name


def main():
    cool_guys = Team(name="cool_guys")

    # Defaults for Nyc will be picked from class
    alice = Contact(name="Alice", age=24, pykw_from=Nyc(door_num=13, street="14th Avenue"))

    bob = Contact(name="Bob", age=25)

    # All properties are optional, does not do validation, so address can be set later
    bob.pykw_from = Address(door_num=14, street="34th Street", city="Toronto", country="Canada")

    # MutableDefault created automatically
    cool_guys.members.append(alice)
    members = cool_guys.members

    # Changes the actual list inside cool_guys like a regular python object
    members.append(bob)

    assert asdict(bob)['from'] == asdict(bob.pykw_from)

    # Prints json
    print(cool_guys)

    # Can construct directly from string or bytes
    cool_guys_copy = Team(str(cool_guys))

    # Prints Alice's door number. IDE autocompletes automatically
    print(cool_guys_copy.members[0].pykw_from.door_num)
    print(cool_guys_copy.members[0].pykw_from.city)
    alice.favfood = {
        "idly": Fav(color="white", qty=10)
    }
    print(alice.favfood["idly"])
    print(alice.favfood["idly"].qty)
    print(alice.favfood.idly.qty)
    for name, fav in data(alice.favfood).items():
        print(name, fav.color)


if __name__ == '__main__':
    main()
