
.ONESHELL:

venv/pyvenv.cfg:
	python3 -m venv venv
	. ./venv/bin/activate
	python3 -m pip install --upgrade pip setuptools wheel twine

latest.whl: venv/pyvenv.cfg jata.py setup.cfg README.md LICENSE
	source ./venv/bin/activate
	python3 setup.py sdist bdist_wheel
	ln -snf dist/python_jata*whl latest.whl

.PHONY: upload
upload: latest.whl
	source ./venv/bin/activate
	# Register on pypi.org, generate token and create a ~/.pypirc file
	# https://packaging.python.org/guides/distributing-packages-using-setuptools/#create-an-account
	twine upload dist/*


.PHONY: build
build: jata.py setup.cfg README.md LICENSE
	python3 setup.py build

.PHONY: clean
clean:
	rm -rf build/ dist/ __pycache__/ python_jata.egg-info/ venv

.PHONY: run
run:
	python3 example.py
